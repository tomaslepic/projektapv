<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    try {
        $stmt = $this->db->prepare("SELECT id_person, nickname, first_name, last_name, birth_day, height, gender FROM person ORDER BY id_person");
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }

    $tplVars['people'] = $stmt->fetchAll();

    return $this->view->render($response, 'index.latte', $tplVars);
})->setName('index');


$app->map(['POST', 'GET'],'/new-person', function (Request $request, Response $response, $args) {

    if ($request->getMethod() == 'GET') {
        return $this->view->render($response, 'new-person.latte', $args);
    } else {
        $data = $request->getParsedBody();
        if (empty($data['first_name']) || empty($data['last_name']) || empty($data['nickname'])) {
            $tplVars['alert'] = 'Vyplňte prosím jméno, příjmení a přezdívku.';

        } else {

            try {
                $stmt = $this->db->prepare(

                    "INSERT INTO person (first_name, last_name, nickname, birth_day, height, gender)
                VALUES (:first_name, :last_name, :nickname, :birth_day, :height, :gender)"

                );


                $stmt->bindValue(':first_name', $data['first_name']);
                $stmt->bindValue(':last_name', $data['last_name']);
                $stmt->bindValue(':nickname', $data['nickname']);
                if (empty($data['birth_day'])){
                    $stmt->bindValue(':birth_day', null);
                } else {
                    $stmt->bindValue(':birth_day', $data['birth_day']);
                }

                if (empty($data['height'])){
                    $stmt->bindValue(':height', null);
                } else {
                    $stmt->bindValue(':height', $data['height']);
                }

                $stmt->bindValue(':gender', $data['gender']);

                $stmt->execute();

                $tplVars["info"] = "Osoba byla přidána";

            } catch (PDOException $e) {
                if($e->getCode() == 23505){
                    $tplVars['alert'] = "Tato osoba již existuje";

                } else {

                    $this->logger->error($e->getMessage());
                    $tplVars['alert'] = "Failed to insert person (" . $e->getMessage() . ")";
                }

            }
        }
    }
        $this->view->render($response, 'new-person.latte', $tplVars);
    })->setName('newPerson');
//TODO unknown value of gender

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');


$app->post('/delete-person', function (Request $request, Response $response, $args) {
    $data = $request->getQueryParams(["id_person"]);

    if (!empty($data["id_person"])) {
        try {
            $stmt = $this->db->prepare("DELETE FROM person WHERE id_person = :id");

            $stmt->bindValue(':id', $data["id_person"]);
            $stmt->execute();
            $tplVars['info'] = "Osoba byla odstraněna";

        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    } else {
        exit('Specify ID of person to delete.');
    }
    $tplVars['info'] = "Osoba odstraněna.";
     return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('deletePerson');

$app->get('/edit-person', function (Request $request, Response $response, $args) {
   // echo "Brace yourself... this feature coming soon 😘";

    $personId = $request->getQueryParam('id');


    if (empty($personId)) {
        exit("Chybí id parametr osoby");
    }
    try {
        $stmt = $this->db->prepare("SELECT * FROM person WHERE id_person = :id_person");
        $stmt->bindValue(':id_person', $personId);
        $stmt->execute();
        $tplVars['person'] = $stmt->fetch();

    } catch (PDOException $e) {
        $this->logger->error($e->getMessage());
        exit("Nelze získat osobu." . $e->getMessage());
    }


    return $this->view->render($response, 'edit-person.latte', $tplVars);

})->setName('editPerson');

$app->post('/edit-person', function (Request $request, Response $response, $args)
{
    $personId = $request->getQueryParam('id');
    if (empty($personId)){
        exit("Chybí id parametr osoby");
    }

    $data = $request->getParsedBody();

    if (empty($data['first_name']) || empty($data['last_name']) || empty($data['nickname'])) {
        $tplVars['alert'] = 'Vyplňte prosím jméno, příjmení a přezdívku';
    } else {
        try {
            $stmt = $this->db->prepare(
              "UPDATE person SET first_name = :first_name, last_name = :last_name,
                  nickname = :nickname, birth_day = :birth_day, gender = :gender, height = :height
                    WHERE id_person = :id_person"
            );
           $stmt->bindValue(':id_person', $personId);
           $stmt->bindValue(':first_name', $data['first_name']);
           $stmt->bindValue(':last_name', $data['last_name']);
           $stmt->bindValue(':nickname', $data['nickname']);
           $stmt->bindValue(':gender', $data['gender']);

           if (empty($data['birth_day'])) {
               $stmt->bindValue(':birth_day', null);
           } else {
               $stmt->bindValue(':birth_day', $data['birth_day']);
           }

           if (empty($data['height']) || empty(intval($data['height']))) {
               $stmt->bindValue(':height', null);
           } else {
               $stmt->bindValue(':height', intval($data['height']));

           }
           $stmt->execute();
           $tplVars['info'] = "Osoba upravena";


        } catch (PDOException $e) {
            $this->logger->error($e->getMessage());
            $tplVars['alert'] = "Chyba při úpravě osoby (" . $e->getMessage() . ")";
        }

    }
    $tplVars['person'] = $data;
    $this->view->render($response, 'edit-person.latte', $tplVars);

});

$app->get('/show-contacts', function (Request $request, Response $response, $args){
    $personId = $request->getQueryParam('id');
    if (empty($personId)){
        $tplVars['alert'] = "Chybí id osoby";
    } else {
        try{

        //$stmt = $this->db->query("SELECT person_id, contact.contact FROM person INNER JOIN contact ON person.id_person=contact.id_person");
        //$stmt = $this->db->query("SELECT nickname FROM person");
            $stmt = $this->db->prepare("SELECT contact_type.name, contact, id_contact FROM contact INNER JOIN contact_type ON contact.id_contact_type=contact_type.id_contact_type WHERE contact.id_person = :id_person");
            $stmt->bindValue(':id_person', $personId);
            $stmt->execute();

            $tplVars['cisla'] = $stmt->fetchAll();
            $tplVars['personid'] = $personId;

        } catch (PDOException $e){
            $this->logger->error($e->getMessage());
            $tplVars['alert'] = "Chyba při zobrazování kontaktních informací";
        }
    }
    $this->view->render($response, 'contact-show.latte', $tplVars);

})->setName('showContacts');

$app->get('/edit-contact', function (Request $request, Response $response, $args){
    $personID=$request->getQueryParams('id_person');
    if (empty($personID)){
        $tplVars['alert'] = "Chybí id osoby";
    } else {
        try {

    $stmt = $this->db->query("SELECT name FROM contact_type");
    $tplVars['contacttype'] = $stmt->fetchAll();
    $this->view->render($response, 'contact-info.latte', $tplVars);
        } catch (PDOException $e) {
            $this->logger->error($e->getMessage());
            $tplVars['alert'] = "Chyba při editaci kontaktních informací";
        }
}

    //TODO znovu sebrání id osoby z kontaktních informací a převzetí do editačního formuláře - hidden element a odesílat postem

})->setName('editContact');

$app->post('/edit-contact', function (Request $request, Response $response, $args){
 //   echo "Brace yourself... this feature coming soon 😘";
    $personId=$request->getParsedBody();
    echo "pridavate tyto informace:";
    foreach ($personId as $item){
        echo $item;
    }
   // $data=$request->getParsedBody();
   // $stmt = $this->db->prepare("UPDATE");
    $tplVars['info'] = $personId;
    //TODO dodělat přidání nových kontaktních informací ke stávajícímu člověku
})->setName('editContact');

$app->get('/delete-contact', function (Request $request, Response $response, $args){
     $personId=$request->getQueryParam('id');
    if (empty($personId)) {
        $tplVars['alert'] = "Chybí id osoby";

    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM contact WHERE id_contact = :id");

            $stmt->bindValue(':id', $personId);
            $stmt->execute();
            $tplVars['info'] = "Kontakt byl odstraněn";


        } catch (PDOException $e) {
            $this->logger->error($e->getMessage());
            $tplVars['alert'] = "Chyba při odstraňování kontaktních údajů";
        }

    }
   // $this->view->render($response, 'contact-show.latte', $tplVars);
 //TODO redirect to contact-show page
})->setName('deleteContact');

$app->get('/relationships-show', function (Request $request, Response $response, $args){

    $this->view->render($response, 'relationships-show.latte');

})->setName('showRelationships');

$app->get('/meetings-show', function (Request $request, Response $response, $args){

    $stmt = $this->db->query("SELECT start, description, duration FROM meeting ORDER BY start");
    $tplVars['meetings'] = $stmt->fetchAll();
    $this->view->render($response, 'meetings-show.latte', $tplVars);

})->setName('showMeetings');

$app->get('/about-app', function (Request $request, Response $response, $args){
    $this->view->render($response, 'about-app.latte');
})->setName('showAboutApp');

$app->post('/meeting-add', function (Request $request, Response $response, $args){
    $meeting=$request->getParsedBody();

    if (empty($meeting)){
        $tplVars['alert'] = "Chybějící údaje";
    } else {
        try {
            $stmt = $this->db->prepare("INSERT INTO meeting (start, description, duration, id_location) VALUES (:start, :description, :duration, :id_location)");
            $stmt->bindValue(':start', $meeting['start']);
            $stmt->bindValue(':description', $meeting['description']);
            $stmt->bindValue(':duration', $meeting['duration']);
            $stmt->bindValue(':id_location', 41);

            $stmt->execute();

        } catch (PDOException $e) {
            $this->logger->error($e->getMessage());
            $tplVars['alert'] = "Nelze přidat schůzku\. Je možné, že daný záznam již existuje\.";
        }
    }
    $tplVars['info'] = "Schůzka úspěšně uložena";
    //TODO - dodělat redirect na meetings-show

})->setName('addMeeting');

$app->get('/show-location', function (Request $request, Response $response, $args){
    $personId = $request->getQueryParam('id');
    if (empty($personId)){
        $tplVars['alert'] = "Chybí id osoby";
    } else {
        try{


            //$stmt = $this->db->prepare("SELECT contact_type.name, contact, id_contact FROM contact INNER JOIN contact_type ON contact.id_contact_type=contact_type.id_contact_type WHERE contact.id_person = :id_person");
            $stmt = $this->db->prepare("SELECT person.id_location, name, street_name, street_number, zip, country 
        FROM location INNER JOIN person ON location.id_location=person.id_location WHERE person.id_person = :id_person ");
            $stmt->bindValue(':id_person', $personId);
            $stmt->execute();

            $tplVars['adresy'] = $stmt->fetchAll();
            $tplVars['personid'] = $personId;

        } catch (PDOException $e){
            $this->logger->error($e->getMessage());
            $tplVars['alert'] = "Chyba při zobrazování kontaktních informací";
        }
    }
    $this->view->render($response, 'location-show.latte', $tplVars);

})->setName('showLocation');


